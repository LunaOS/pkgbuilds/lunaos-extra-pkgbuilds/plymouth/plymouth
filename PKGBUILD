pkgname=plymouth
pkgname=plymouth
pkgver=24.004.60
pkgrel=4
pkgdesc='Graphical boot splash screen'
arch=(x86_64)
url='https://www.freedesktop.org/wiki/Software/Plymouth/'
license=(GPL-2.0-or-later)
provides=(plymouth plymouth=$pkgver lunaos-plymouth)
conflicts=(lunaos-plymouth)
replaces=(lunaos-plymouth)
depends=(
  bash
  cairo
  cantarell-fonts
  filesystem
  fontconfig
  freetype2
  glib2
  glibc
  libdrm
  libevdev
  libpng
  libx11
  libxkbcommon
  pango
  systemd-libs
  xkeyboard-config
  lunaos-plymouth-theme
)
makedepends=(
  docbook-xsl
  git
  gtk3
  meson
)
optdepends=('gtk3: x11 renderer')
backup=(etc/plymouth/plymouthd.conf)
install='plymouth.install'
source=(
  "git+https://gitlab.freedesktop.org/plymouth/$pkgname.git#tag=$pkgver"
  plymouth.initcpio_hook
  plymouth.initcpio_install
  plymouth-shutdown.initcpio_install
  mkinitcpio-generate-shutdown-ramfs-plymouth.conf
  001-lunaos-add-more-initrd-to-plymouth-update-initrd
)

b2sums=('a3d55f4f7be81bdf2ddd5c2b74a3fdb4e368c31fc41e12ab100ce2a7986cb418151b3df0d0316011710dd0e1ae99631166eecf80bc1dd5cc9054a4685266afed'
        'afb2449b542aa3e971eab6b953c907347fdf4e499b4140a5e6736a7c99557c0d8d2fed28dbee56d84c8c619335c59bd382457d85e51145884ad0616e9095f232'
        '91f9e2864a73fa3884cd0074cbb7c9535ab83308466fe8994691cc8aab9b6001b0339c9eb9766c6084758112d87723c11f22f1e55f420f34ce2ddcc68ae35b38'
        '063448411de837ed38ece935719f07fd17b18830680c9fa95b7bd39a097778186c40373590504c9b44144125986304311f528c73592c29d19043b8395e6f99c2'
        '7bb910b7402ad4372f1918be763421308534044c46d42e77af0aadcb7cbe13a99633805b36fa841f8ea1006ddb204ecea3031b96b05ec4e8d16150b2c7864629'
        '54d4aafe0699cda832693365f4f7f7b2051e07c0da0055164376a1b7d601e70adf23b6b3bef14ce37d5789a1a6673d93bc5394d07be007280ad879dd14b49b2a')

prepare() {
  cd "$pkgname"

  # Various fixes from upstream
  git cherry-pick -n -m 1 24.004.60..c08a22599f595915e39a1a900c5eb86c967a15e5

  patch -Np1 -i ../001-lunaos-add-more-initrd-to-plymouth-update-initrd
  
  # Change default theme
  sed -i 's/^Theme=spinner$/Theme=lunaos-theme/' src/plymouthd.defaults
}

build() {
  arch-meson $pkgname build \
    -D logo=/usr/share/pixmaps/lunaos-logo.png
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
  rm -r "$pkgdir/run"

  # Install mkinitcpio hook
  install -Dm644 plymouth.initcpio_hook "$pkgdir/usr/lib/initcpio/hooks/$pkgname"
  install -Dm644 plymouth.initcpio_install "$pkgdir/usr/lib/initcpio/install/$pkgname"

  # Install mkinitcpio shutdown hook and systemd drop-in snippet
  install -Dm644 plymouth-shutdown.initcpio_install "$pkgdir/usr/lib/initcpio/install/$pkgname-shutdown"
  install -Dm644 mkinitcpio-generate-shutdown-ramfs-plymouth.conf "$pkgdir/usr/lib/systemd/system/mkinitcpio-generate-shutdown-ramfs.service.d/plymouth.conf"
}
